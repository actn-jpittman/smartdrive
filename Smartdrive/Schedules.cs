﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Smartdrive
{
    public static class Schedules
    {
        public static List<Driver_Schedule> GetDriverSchedules(DateTime startdate, DateTime enddate)
        {
            List<Driver_Schedule> schedules = new List<Driver_Schedule>();


            using (SqlConnection con = new SqlConnection("data source=action-sql;initial catalog=lme_1720;persist security info=True;user id=reports;password=reports;MultipleActiveResultSets=True;App=EntityFramework"))
            {
                SqlCommand cmd = new SqlCommand(string.Format(@"
                    SELECT c.actual_arrival AS orig_arrival, c.dest_actualarrival AS dest_arrival, t.fleet_id, 
                    CASE
                        WHEN fleet_id = '01' THEN 'Hanceville'
                        WHEN fleet_id = '02' THEN 'Mobile'
                        WHEN fleet_id = '03' THEN 'Gadsden'
                        WHEN fleet_id = '14' THEN 'Pasadena'
                        WHEN fleet_id = '16' THEN 'Cordele'
                        WHEN fleet_id = '18' THEN 'St.Gabriel'
                        WHEN fleet_id = '20' THEN 'Charlotte'
                        WHEN fleet_id = '21' THEN 'Columbus'
                        WHEN fleet_id = '22' THEN 'McAlester'
                        WHEN fleet_id = '23' THEN 'Salt Lake City'
                        WHEN fleet_id = '24' THEN 'Phoenix'
                        WHEN fleet_id = '26' THEN 'Pendergrass'
                        WHEN fleet_id = '28' THEN 'Kimball'
                        ELSE 'UNKNOWN' END AS location,
                    m.id AS movement_id, t.id AS tractor_id, t.driver1_id,
                    RTRIM(d.name) AS last_name, RTRIM(d.first_name) AS first_name, ISNULL(RTRIM(d.name_mid_initial), '') AS name_mid_initial
                    FROM continuity c
                    JOIN movement m ON m.id = c.movement_id AND m.company_id = c.company_id
                    JOIN tractor t ON t.id = c.equipment_id AND t.company_id = c.company_id
                    JOIN driver d ON d.id = t.driver1_id AND d.company_id = c.company_id
                    WHERE c.company_id = 'TMS'
                    AND (c.actual_arrival > '{0}' AND
                        c.actual_arrival < '{1}')
                    AND t.fleet_id IS NOT NULL
                    AND c.actual_arrival Is NOT NULL
                    AND c.dest_actualarrival Is NOT NULL
                    ", startdate.ToString("M/d/yyyy HH:mm:ss"), 
                        enddate.ToString("M/d/yyyy HH:mm:ss")) , con);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int i = 0;

                while (reader.Read())
                {
                    schedules.Add(new Driver_Schedule()
                    {

                        DriverSite              = reader["location"].ToString().Trim(),
                        EmployeeId              = reader["driver1_id"].ToString().Trim(),
                        EndDateTime             = DateTime.Parse(reader["dest_arrival"].ToString().Trim()).ToString("s"),
                        StartDateTime           = DateTime.Parse(reader["orig_arrival"].ToString().Trim()).ToString("s"),
                        FirstName               = reader["first_name"].ToString().Trim(),
                        LastName                = reader["last_name"].ToString().Trim(),
                        MiddleInitial           = reader["name_mid_initial"].ToString(),
                        ScheduleCreationTime    = DateTime.Now.ToString(), VehicleSN= reader["tractor_id"].ToString().Trim()

                    });

                }
            }
            return schedules;

        }

    }
    

    public class Driver_Schedule
    {
        public string ScheduleCreationTime { get; set; }
        public string DriverSite { get; set; }
        public string EmployeeId { get; set; }
        public string VehicleSN { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
    }

    public class Truck_Response
    {
        public string SiteName { get; set; }
        public string VehicleSN { get; set; }
        public string VIN { get; set; }
        public string VehicleType { get; set; }
        public string FuelType { get; set; }
        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string SmartRecorderSN { get; set; }
        public string SmartRecorderStatusCode { get; set; }
        public string StatusReasonCode { get; set; }
        public string LapBelt { get; set; }
        public string ConcurrencyHash { get; set; }
        public string PostedSpeedClass { get; set; }
    }
}
