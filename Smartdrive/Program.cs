﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Smartdrive
{
    class Program
    {
        static public async Task<string> PostDriverTest(List<Driver_Schedule> ds)
        {
            var serializeddata = JsonConvert.SerializeObject(ds);
            var content = new StringContent(serializeddata, Encoding.UTF8, "application/json");


            string url = @"https://ws.smartdrive.net/DriverScheduleService/api/driverscheduleservice/driverSchedules";
            string accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiOTkyLDExNDIxLDE1MTAzMTMsMjAxOS0wMy0yMVQxMjowNTo1Ny4zODEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTU4NDgzNTE5OSwibmJmIjoxNTUzMTI2NDAwfQ.AJs2Cw4GDOQ9g5UiQM3eziPbr2cILntk145fCIPSLBw";
            string test_accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiNDk2LDEwNzk2LDE0Njg2MzUsMjAxOS0wMy0yMVQxMzoyMjoyMC41MDEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTYxNjE5ODM5OSwibmJmIjoxNTUzMTI2NDAwfQ.3tR9LlxLmWhVkXCv_zMlPzZgvHSicPnbip10LY1i2sI";
            string response = "";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var resp = await client.PostAsync(url, content);

                if (resp.ToString().Contains("StatusCode: 400"))
                {
                    Console.WriteLine("bad.");
                }

                response = resp.RequestMessage.ToString();
            }

            return response;
        }


        // Simple async function returning a string...
        //static public async Task<string> CallHttp()
        //{
        //    string url = @"https://ws-test.smartdrive.net/VehicleManagement/?SiteName=Action Resources";
        //    string accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiOTkyLDExNDIxLDE1MTAzMTMsMjAxOS0wMy0yMVQxMjowNTo1Ny4zODEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTU4NDgzNTE5OSwibmJmIjoxNTUzMTI2NDAwfQ.AJs2Cw4GDOQ9g5UiQM3eziPbr2cILntk145fCIPSLBw";
        //    string test_accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiNDk2LDEwNzk2LDE0Njg2MzUsMjAxOS0wMy0yMVQxMzoyMjoyMC41MDEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTYxNjE5ODM5OSwibmJmIjoxNTUzMTI2NDAwfQ.3tR9LlxLmWhVkXCv_zMlPzZgvHSicPnbip10LY1i2sI";
        //    string response = "";

        //    using (var client = new HttpClient())
        //    {
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", test_accessToken);

        //        //client.DefaultRequestHeaders.Add("Authorization", accessToken);
        //        response = await client.GetStringAsync(url);
        //    }

        //    return response;

        //}

        static public async Task<string> GetDrivers(string site, DateTime startdate, DateTime enddate)
        {
            //string url = @"https://ws-test.smartdrive.net/VehicleManagement/?SiteName=Action%20Resources";
            //string url = @"https://ws-test.smartdrive.net/userservice/api/userservice/drivers?siteName=Action%20Resources";
            //string url = @" https://ws-test.smartdrive.net/DriverScheduleService/api/driverscheduleservice/driverSchedules/2019-03-31/2019-04-03/?SiteName=Mobile"; ///driverSchedules/2019-03-21/2019-03-22/?siteName=Action Resources";
            string url = string.Format(@" https://ws.smartdrive.net/DriverScheduleService/api/driverscheduleservice/driverSchedules/{0}/{1}/?SiteName={2}",
                startdate.ToString("yyyy-MM-dd"),
                enddate.ToString("yyyy-MM-dd"),
                site);
            //
            string accessToken =      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiOTkyLDExNDIxLDE1MTAzMTMsMjAxOS0wMy0yMVQxMjowNTo1Ny4zODEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTU4NDgzNTE5OSwibmJmIjoxNTUzMTI2NDAwfQ.AJs2Cw4GDOQ9g5UiQM3eziPbr2cILntk145fCIPSLBw";
            string test_accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiNDk2LDEwNzk2LDE0Njg2MzUsMjAxOS0wMy0yMVQxMzoyMjoyMC41MDEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTYxNjE5ODM5OSwibmJmIjoxNTUzMTI2NDAwfQ.3tR9LlxLmWhVkXCv_zMlPzZgvHSicPnbip10LY1i2sI";
            string response = "";

            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = await client.GetStringAsync(url);

                //client.DefaultRequestHeaders.Add("Authorization", accessToken);
                //response = await client.GetStringAsync(url);
            }

            return response;

        }





        //public class RootObject
        //{
        //    public List<Truck_Response> Products { get; set; }
        //}

        //static public async Task<string> DriverTest()
        //{
        //    //string url = @"https://ws-test.smartdrive.net/VehicleManagement/?SiteName=Action%20Resources";
        //    string url = @"https://ws-test.smartdrive.net/DriverScheduleService/api/driverscheduleservice/version";
        //    //string url = @"https://ws-test.smartdrive.net/userservice/api/userservice/drivers/?siteName=Action%20Resources";
        //    //string url = @" https://ws-test.smartdrive.net/DriverScheduleService/api/driverscheduleservice/driverSchedules/2019-03-31/2019-04-01/"; ///driverSchedules/2019-03-21/2019-03-22/?siteName=Action Resources";
        //    //
        //    string accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiOTkyLDExNDIxLDE1MTAzMTMsMjAxOS0wMy0yMVQxMjowNTo1Ny4zODEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTU4NDgzNTE5OSwibmJmIjoxNTUzMTI2NDAwfQ.AJs2Cw4GDOQ9g5UiQM3eziPbr2cILntk145fCIPSLBw";
        //    string test_accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiNDk2LDEwNzk2LDE0Njg2MzUsMjAxOS0wMy0yMVQxMzoyMjoyMC41MDEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTYxNjE5ODM5OSwibmJmIjoxNTUzMTI2NDAwfQ.3tR9LlxLmWhVkXCv_zMlPzZgvHSicPnbip10LY1i2sI";
        //    string response = "";

        //    using (var client = new HttpClient())
        //    {
        //        //client.BaseAddress = new Uri(url);
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", test_accessToken);
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        HttpResponseMessage resp = client.GetAsync(url).Result;


        //        //client.DefaultRequestHeaders.Add("Authorization", accessToken);
        //        //response = await client.GetStringAsync(url);
        //    }

        //    return response;
        //}

        public static List<Truck_Response> JsonParseTractors(string jsonText)
        {
            return JsonConvert.DeserializeObject<Truck_Response[]>(jsonText).ToList();
        }

        public static List<Driver_Schedule> JsonParseDriverSchedules(string jsonText)
        {
            return JsonConvert.DeserializeObject<RootObject_DriverSchedule>(jsonText).Schedules;
        }

        public class RootObject_DriverSchedule
        {
            [JsonProperty("DriverSchedules")]
            public List<Driver_Schedule> Schedules { get; set; }
        }

        public class RootObject_Tractor
        {
            [JsonProperty("")]
            public List<Truck_Response> Trucks { get; set; }
        }

        private static List<string> GetVehicles(string site)
        {
            //string url = @"https://ws-test.smartdrive.net/VehicleManagement/?SiteName=Action%20Resources";
            string url = string.Format(@"https://ws.smartdrive.net/VehicleManagement/?SiteName={0}", site);
            //string url = @"https://ws-test.smartdrive.net/userservice/api/userservice/drivers/?siteName=Action%20Resources";
            //string url = @" https://ws-test.smartdrive.net/DriverScheduleService/api/driverscheduleservice/driverSchedules/2019-03-31/2019-04-01/"; ///driverSchedules/2019-03-21/2019-03-22/?siteName=Action Resources";
            //
            string accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiOTkyLDExNDIxLDE1MTAzMTMsMjAxOS0wMy0yMVQxMjowNTo1Ny4zODEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTU4NDgzNTE5OSwibmJmIjoxNTUzMTI2NDAwfQ.AJs2Cw4GDOQ9g5UiQM3eziPbr2cILntk145fCIPSLBw";
            string test_accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJTRFNJIjoiNDk2LDEwNzk2LDE0Njg2MzUsMjAxOS0wMy0yMVQxMzoyMjoyMC41MDEiLCJpc3MiOiJTRFRva2VuU2VydmljZSIsImV4cCI6MTYxNjE5ODM5OSwibmJmIjoxNTUzMTI2NDAwfQ.3tR9LlxLmWhVkXCv_zMlPzZgvHSicPnbip10LY1i2sI";
            string response = "";
            List<string> trucks = new List<string>();

            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    string resp = client.GetStringAsync(url).Result;

                    var res = JsonParseTractors(resp);

                    foreach (var t in res)
                        trucks.Add(t.VehicleSN);
                    Console.WriteLine("Vehicles Loaded: {0}", site);
                    return trucks;

                }
                catch (Exception xx)
                {
                    Console.WriteLine("Vehicles Failed to Load: {0}", site);
                    return new List<string>();
                }
                //client.DefaultRequestHeaders.Add("Authorization", accessToken);
                //response = await client.GetStringAsync(url);
            }

        }

        static void Main(string[] args)
        {
            DateTime startdate = DateTime.Now.AddDays(-7);
            DateTime enddate = DateTime.Now;

            //DateTime startdate = new DateTime(2019, 3, 6);
            //DateTime enddate = new DateTime(2019, 4, 4);

            List<string> Sites = new List<string>();
            Sites.Add("Mobile");
            //Sites.Add("Hanceville");
            //Sites.Add("Gadsden");
            //Sites.Add("Pasadena");
            //Sites.Add("Cordele");
            //Sites.Add("St.Gabriel");
            //Sites.Add("Charlotte");
            //Sites.Add("Columbus");
            //Sites.Add("McAlester");
            //Sites.Add("Salt Lake City");
            //Sites.Add("Phoenix");
            //Sites.Add("Pendergrass");
            //Sites.Add("Kimball");
            List<Driver_Schedule> API_Schedule = new List<Driver_Schedule>();
            List<string> tractors = new List<string>();

            foreach (string site in Sites)
            {
                tractors.AddRange(GetVehicles(site));

                // Start a task - calling an async function in this example
                Task<string> calldrvTask = Task.Run(() => GetDrivers(site, startdate, enddate)); // CallHttp());

                try
                {
                    // Wait for it to finish
                    calldrvTask.Wait();
                    // Get the result
                    string astr = calldrvTask.Result;
                    // Write it our
                    //Console.WriteLine(astr);

                    //JObject h = JObject.Parse(astr);
                    API_Schedule.AddRange(JsonParseDriverSchedules(astr));


                    Console.WriteLine("Site Load Complete: {0}", site);
                }
                catch (Exception xx)
                {
                    Console.WriteLine("Site Failed: {0}", site);
                }
            }

            var McLeod_Schedule = Schedules.GetDriverSchedules(
                startdate,
                enddate
                );

            //try
            //{
            // Start a task - calling an async function in this example
            //Task<string> callTask = Task.Run(() => GetDrivers()); // CallHttp());
            // Wait for it to finish
            //callTask.Wait();
            // Get the result
            //string astr = callTask.Result;
            // Write it our
            //Console.WriteLine(astr);

            //JObject h = JObject.Parse(astr);
            //var API_Schedule = JsonParseDriverSchedules(astr);

            //foreach (var d in API_Schedule)
            //    if (!tractors.Contains(d.VehicleSN))
            //        tractors.Add(d.VehicleSN);

            //var API_Schedule = JsonConvert.DeserializeObject<Driver_Schedule>(astr);


            //var test = McLeod_Schedule.Where(d => d.EmployeeId=="MOSTRA").ToList();

            //var match_test = API_Schedule.Where(d =>
            //    d.VehicleSN == "129060" &&
            //    d.EmployeeId == "MOSTRA").ToList();

            for (int x = McLeod_Schedule.Count-1;x>=0;x--)
                //foreach (var m_drv_sched in McLeod_Schedule)
                {

                    var m_drv_sched = McLeod_Schedule[x];

                //    if (m_drv_sched.VehicleSN=="129062")
                //{
                //    Console.WriteLine("here");

                //    var match_test = API_Schedule.Where(d =>
                //        d.DriverSite == m_drv_sched.DriverSite &&
                //        d.VehicleSN == m_drv_sched.VehicleSN).ToList();
                //}


                    var match = API_Schedule.Where(d =>
                        d.DriverSite == m_drv_sched.DriverSite &&
                        d.EmployeeId == m_drv_sched.EmployeeId &&
                        d.StartDateTime == m_drv_sched.StartDateTime &&
                        d.EndDateTime == m_drv_sched.EndDateTime).Count();

                    if (match > 0 || !tractors.Contains(m_drv_sched.VehicleSN)) // || (DateTime.Parse(m_drv_sched.EndDateTime) <= DateTime.Parse(m_drv_sched.StartDateTime)))
                        McLeod_Schedule.RemoveAt(x);
                }

            //Task<string> callTask = Task.Run(() => PostDriverTest(McLeod_Schedule)); //  McLeod_Schedule));
            //callTask.Wait();
            //string apoststr = callTask.Result;

            for (int x = 0; x < McLeod_Schedule.Count; x++)
            {
                List<Driver_Schedule> test_ds = new List<Driver_Schedule>();
                test_ds.Add(McLeod_Schedule[x]);

                try
                {
                    Task<string> callTask = Task.Run(() => PostDriverTest(test_ds)); //  McLeod_Schedule));
                    callTask.Wait();
                    string apoststr = callTask.Result;
                }
                catch (Exception xx)
                {
                    Console.WriteLine(xx.Message.ToString());
                }
            }



            //string pattern = @"((?<=VehicleSN"":"")(.*?)(?=""))";
            //Regex rgx = new Regex(pattern);
            //foreach (Match match in rgx.Matches(astr))
            //{
            //    tractors.Add(match.Value);
            //    //Console.WriteLine("Found '{0}' at position {1}", match.Value, match.Index);
            //}

            //}
            //catch (Exception ex)  //Exceptions here or in the function will be caught here
            //{
            //    Console.WriteLine("Exception: " + ex.Message);
            //}
        }
    }
}
